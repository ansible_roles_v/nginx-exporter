from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_files(host):
    files = [
        "/etc/systemd/system/nginx-exporter.service",
        "/usr/local/bin/nginx_exporter"
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file

def test_user(host):
    assert host.group("nginxexporter").exists
    assert "nginxexporter" in host.user("nginxexporter").groups
    assert host.user("nginxexporter").shell == "/usr/sbin/nologin"

def test_service(host):
    s = host.service("nginx-exporter.service")
    assert s.is_enabled
