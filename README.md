Nginx Exporter
==============
Installs Nginx Prometheus Exporter.  

Include role
------------
```yaml
- name: nginx_exporter  
  src: https://gitlab.com/ansible_roles_v/nginx-exporter/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: hosts
  gather_facts: true
  become: true
  roles:
    - nginx_exporter
```